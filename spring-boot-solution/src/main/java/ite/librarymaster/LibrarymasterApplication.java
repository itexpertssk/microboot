package ite.librarymaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibrarymasterApplication {

	public static void main(String[] args) {
		SpringApplication.run(LibrarymasterApplication.class, args);
	}
}
