package ite.librarymaster.domain.model;

/**
 * Defines Library Medium aviability enumeration.
 * @author ivan.macalak@posam.sk
 *
 */
public enum MediumAvailability {
	Available,
	Borrowed,
	Archive
}
