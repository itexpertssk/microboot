package ite.librarymaster.domain.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Book Repository interface defines operations to
 * access and manipulate Books.
 * 
 * @author ivan.macalak@posam.sk
 *
 */
@Repository
public interface BookRepository extends JpaRepository<Book,Long> {
	
	Book findByIsbn(String isbn);

}
