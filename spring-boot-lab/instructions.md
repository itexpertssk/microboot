This Web application implements simple Library functionality which is presented by the HTML pages (Thymeleaf)
and also exposes functionality via RESTfull style web services. It utilizes the Spring MVC ans other Spring modules
as Spring Data.
The goal is to rewrite current implementation using the Spring Boot. The Spring Boot brings great simplification
in areas of project dependency management, application configuration and initialization.

1.) Application is distributed as Java Web Archive WAR which should be deployed on Servlet container,
    Apache Tomcat for instance. First build he war by running `.\gradlew war` command from the project's root
    directory. The resulting war is placed into _build_gradle\libs_ directory. Run the Tomcat server and
    deploy the war. Now point your browser to [http://localhost:8080/spring-boot-lab/index.html](http://localhost:8080/spring-boot-lab/index.html).
    The REST API is available at [http://localhost:8080/spring-boot-lab/api/books](http://localhost:8080/spring-boot-lab/api/books) 
    endpoint.    
    
2.) Let's rewrite the application using the Spring Boot approach. You should start with the build.gradle build script.
    The best is to use the [Spring initializer](https://start.spring.io/) website, where you can define the Spring Boot 
    starters, you want to use in your application. So, open the website and add following Dependencies: _Web, JPA, H2_ and _Thymeleaf_.
    Type Group as _ite_ and Artifact as _librarymaster_ (or you can choose your Group and Artifact). Then choose _Gradle_ project 
    (or Maven, if Maven is your you preferred build tool), **Java** and Spring boot version _2.3.5_. Now click the **Generate** button and
    save resulting zip file.
    Your setup should looks like:
    ![initializer](docs/images/spring-initializer.PNG)   
    
3.) Now, unzip and copy content of the downloaded librarymaster.zip into this project. Some files will be replaced.
    The Intellij IDEA ide will reimport the project automatically. The most important file is the build.gradle file. 
    Open the build.gradle and review it. Notice that dependencies have no versions. There is only the Spring Boot plugin
    version defined (2.3.5.RELEASE). There is also _ite.librarymaster.LibrarymasterApplication_ bootstrap class created.
    As the Spring Boot uses the embedded Tomcat Application server, you can start application from the _LibrarymasterApplication_
    class. The _LibrarymasterApplication_ class is annotated by the _@SpringBootApplication_, which does all the magic of the app configuration
    after start. Review also jars added on the project's classpath. This application is distributed as executable fat JAR file
    where all required libraries (including Tomcat AS) are included. The resource src/main/java/resource project directory
    now contains the _application.properties_ where you can define the app configuration parameters, and there is also _static_ folder,
    where the web application static resources should be placed. Next, you should change some existing source code and configuration.
    There is one more thing. The web layer uses Bootstrap, JQuery and some Fonts, so you need to add additional dependencies into build.gradle: 
    
    implementation("org.webjars:bootstrap:3.3.6")
    implementation("org.webjars:jquery:2.2.0")
    implementation("org.webjars:font-awesome:4.5.0")        
    
4.) You can simplify the persistence layer. Open the _ite.librarymaster.domain.model.BoorRepository_ interface and
    extend it from the _JpaRepository<Book,Long>_ Spring data-jpa interface. Annotate the BookRepository class with the
    @Repository annotation. You can now remove all interface methods, but the_Book findByIsbn(String isbn);_ As the 
    Spring Data JPA generates implementation for you, you can remove the _ite.librarymaster.infrastructure.persistence.JpaBookRepository_ 
    class completely. As you added the H2 database as dependency, the Spring Boot configures the default _datasource_ for you and starts 
    embedded H2 database on application start. (you can configure different datasource according your needs later)
    The last thing is to change annotation of abstract model classes _Medium_ and _Text_ from _@Entity_ to _@MappedSuperclass_. 
    Now the BOOK table instead of the MEDIUM table will be created in DB.    

5.) Now, open the _ite.librarymaster.application.service.LibraryServiceImpl_ class and fix few issues. First remove the 
    `@Qualifier("jpaBookRepository")`. Then the `bookRepository.saveBook(book)` should be replaced by `bookRepository.save(book)`. 
    The `bookRepository.findById(id)` now return _Optional_, so replace code of the _getBookById_ method by: 
    
    Optional<Book> book = bookRepository.findById(id);
    if(book.isPresent()){
      return book.get();
    }else {
      throw new ItemNotFoundException("Book with id=" + id + " not found.");
    }
    
6.) Next, fix the web layer. Move the _src/main/webapp/resources_ directory under the _src/main/resources/static_ directory. 
    Move the _src/main/webapp/views_ directory and the _src/main/webapp/index.html_ file under the _src/main/resources_ directory. 
    Now remove the _src/main/webapp_ directory. You do not need the Spring xml configuration files anymore. Now configure the Thymeleaf engine in 
    _application.properties_ file:
    
    spring.thymeleaf.cache=false
    spring.thymeleaf.enabled=true
    spring.thymeleaf.prefix=classpath:/views/
    spring.thymeleaf.suffix=.html
    spring.thymeleaf.encoding=UTF-8
    spring.thymeleaf.mode=HTML5
    
7.) To enable JPA to create the DB schema during app start and to show generated SQL add following properties into
    application.properties_ file:
    
    spring.jpa.hibernate.ddl-auto=create-drop
    spring.jpa.show-sql=true
    spring.h2.console.enabled=true
    
8.) Now, you can start the application and check the result: [Web app](http://localhost:8080/home), [API](http:localhost:8080/api/books)
         
     
  
    
      