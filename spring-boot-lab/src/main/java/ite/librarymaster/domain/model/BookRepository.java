package ite.librarymaster.domain.model;

import ite.librarymaster.domain.model.Book;

import java.util.List;

/**
 * Book Repository interface defines operations to
 * access and manipulate Books.
 * 
 * @author ivan.macalak@posam.sk
 *
 */
public interface BookRepository {
	
	List<Book> findAll();
	Book findById(Long id);
	Book findByIsbn(String isbn);
	void saveBook(Book book);

}
