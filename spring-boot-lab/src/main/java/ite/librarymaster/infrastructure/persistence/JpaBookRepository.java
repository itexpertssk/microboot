package ite.librarymaster.infrastructure.persistence;

import ite.librarymaster.domain.model.Book;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ite.librarymaster.domain.model.BookRepository;
import org.springframework.stereotype.Repository;

@Repository("jpaBookRepository")
public class JpaBookRepository implements BookRepository {
	
	@PersistenceContext
	EntityManager entityManager; 

	@Override
	public List<Book> findAll() {
		List<Book> result=entityManager.createNamedQuery("book.findAll",Book.class).getResultList();
		return result;
	}

	@Override
	public Book findByIsbn(String isbn) {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public Book findById(Long id) {
        return entityManager.find(Book.class, id);
    }

	@Override
	public void saveBook(Book book) {
		if(book.getId() != null){
			entityManager.merge(book);
		} else {
			entityManager.persist(book);
		}

	}


}
