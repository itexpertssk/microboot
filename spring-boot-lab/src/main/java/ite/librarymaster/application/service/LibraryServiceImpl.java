package ite.librarymaster.application.service;

import ite.librarymaster.application.exception.ItemNotFoundException;
import ite.librarymaster.domain.model.BookRepository;
import ite.librarymaster.domain.model.Book;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.*;

@Service
@Transactional
public class LibraryServiceImpl implements LibraryService{
	
	@Autowired
	@Qualifier("jpaBookRepository")
	private BookRepository bookRepository;

	@Override
	@Transactional(readOnly=true)
	public List<Book> getAllBooks() {
		return bookRepository.findAll();
	}

    @Override
    @Transactional(readOnly=true)
    public Book getBookById(Long id) throws ItemNotFoundException {
        Book book = bookRepository.findById(id);
        if(book == null){
            throw new ItemNotFoundException("Book with id="+id+" not found.");
        }
        return book;
    }

    @Override
    public Book saveBook(Book book) {
        bookRepository.saveBook(book);
        return book;
    }
}
