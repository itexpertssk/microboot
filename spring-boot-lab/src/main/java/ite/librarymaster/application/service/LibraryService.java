package ite.librarymaster.application.service;

import ite.librarymaster.application.exception.ItemNotFoundException;
import ite.librarymaster.domain.model.Book;

import java.util.List;

public interface LibraryService {
	
	List<Book> getAllBooks();
	Book getBookById(Long id) throws ItemNotFoundException;
	Book saveBook(Book book);

}
